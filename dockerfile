FROM ubuntu:18.04
LABEL "AUTHOR"="Divya Reddy"
RUN apt-get update
RUN apt-get install apache2 -y 
COPY index.html /var/www/html
EXPOSE 40
CMD ["apachectl", "-D", "FOREGROUND"]
